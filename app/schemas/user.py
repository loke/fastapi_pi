from pydantic import BaseModel


class UserBase(BaseModel):
    username: str


class UserIn(UserBase):
    hashed_password: str


class User(UserBase):
    id: int
    hashed_password: str

    class Config:
        orm_mode = True
