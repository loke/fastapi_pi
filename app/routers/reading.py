from datetime import datetime
from typing import List

from fastapi import APIRouter, Depends, Query
from sqlalchemy.orm import Session

from app.authenticate import oauth2_scheme
from app.db import get_db
from app.schemas.reading import Reading
from app.services.reading import get_readings, get_readings_by_date

router = APIRouter(prefix='/readings')


@router.get('', response_model=List[Reading])
def read_readings(skip: int = Query(0, ge=0),
                  limit: int = Query(100, ge=1),
                  db: Session = Depends(get_db),
                  token: str = Depends(oauth2_scheme)):
    readings = get_readings(db, skip=skip, limit=limit)
    return readings


@router.get('/', response_model=List[Reading])
def read_readings(start: datetime = Query(...),
                  end: datetime = Query(...),
                  skip: int = 0, limit: int = 100,
                  db: Session = Depends(get_db), token: str = Depends(oauth2_scheme)):
    readings = get_readings_by_date(db, start, end, skip=skip, limit=limit)
    return readings
