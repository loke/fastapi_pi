import schedule

from app.readings.temperature import save_temperature


def init_temp_task():
    schedule.every(5).minutes.do(save_temperature).tag('5_min_temp')


def clear_temp_task():
    schedule.clear('5_min_temp')
