from datetime import datetime

from sqlalchemy.orm import Session

from app.models.reading import Reading


def get_readings(db: Session, skip: int = 0, limit: int = 100):
    return db.query(Reading).offset(skip).limit(limit).all()


def get_readings_by_date(db: Session, start: datetime, end: datetime,
                         skip: int = 0, limit: int = 100):
    return db.query(Reading).filter(
        Reading.timestamp >= start, Reading.timestamp <= end
    ).offset(skip).limit(limit).all()
