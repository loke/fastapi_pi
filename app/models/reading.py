from sqlalchemy import Column, DateTime, Float, ForeignKey, Integer
from sqlalchemy.orm import relationship

from app.db import Base


class Reading(Base):
    __tablename__ = 'readings'

    id = Column(Integer, primary_key=True, index=True)
    timestamp = Column(DateTime, unique=True)
    temperature = Column(Float, default=10.0)
    owner_id = Column(Integer, ForeignKey('owners.id'))

    owner_system = relationship('Owner', back_populates='readings')
