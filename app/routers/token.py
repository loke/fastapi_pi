from fastapi import APIRouter, Depends
from fastapi.security import OAuth2PasswordRequestForm
from sqlalchemy.orm import Session

from app.authenticate import get_token
from app.db import get_db

router = APIRouter(prefix='/token')


@router.post('')
def login(form_data: OAuth2PasswordRequestForm = Depends(),
          db: Session = Depends(get_db)):
    return get_token(db, form_data)
