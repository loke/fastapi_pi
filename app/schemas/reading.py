from datetime import datetime

from pydantic import BaseModel


class ReadingBase(BaseModel):
    timestamp: datetime
    temperature: float


class ReadingIn(ReadingBase):
    pass


class Reading(ReadingBase):
    id: int
    owner_id: int

    class Config:
        orm_mode = True
