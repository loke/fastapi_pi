import time
import json
import os
import subprocess

from app.db import get_db
from app.schemas.reading import ReadingIn
from app.services.owner import create_owner_reading, get_owner_by_name

commands = (
    # Manjaro
    # ['sensors', '-j'],

    # Pi
    # ['vcgencmd', 'measure_temp'],
    ['cat', '/sys/class/thermal/thermal_zone0/temp'],
)
db = next(get_db())


def read_temperature() -> float:
    temp = None
    for command in commands:
        # print(f'{command=}')
        try:
            proc = subprocess.run(command, stdout=subprocess.PIPE, stderr=subprocess.DEVNULL)
            stdout = proc.stdout.decode('utf-8')
            # stderr = proc.stderr.decode('utf-8')
            # output = json.loads(stdout)
            # temp = round(output['nvme-pci-0500']['Composite']['temp1_input'], 1)
            temp = round(float(stdout) / 1000, 1)
        except (FileNotFoundError, ValueError):
            pass  # print('no command found:', command)
        except json.decoder.JSONDecodeError as e:
            pass  # print(f'JSON error: {e}')
    if temp:
        return temp
    else:
        raise Exception('Couldn\'t get temperature.')


def save_temperature():
    host = os.uname().nodename
    owner = get_owner_by_name(db, host)
    if not owner:
        raise Exception(f'Couldn\'t find owner {host}')
    now = time.strftime('%Y-%m-%d %H:%M:%S')
    reading = ReadingIn(timestamp=now, temperature=read_temperature())
    create_owner_reading(db, reading, owner.id)
