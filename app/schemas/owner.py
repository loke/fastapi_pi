from typing import List

from pydantic import BaseModel

from app.schemas.reading import Reading


class OwnerBase(BaseModel):
    name: str


class OwnerIn(OwnerBase):
    pass


class Owner(OwnerBase):
    id: int
    is_active: bool
    readings: List[Reading] = []

    class Config:
        orm_mode = True
