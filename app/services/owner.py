from sqlalchemy.orm import Session

from app.models.owner import Owner
from app.models.reading import Reading
from app.schemas.owner import OwnerIn
from app.schemas.reading import ReadingIn


def create_owner(db: Session, owner: OwnerIn):
    db_owner = Owner(name=owner.name)
    db.add(db_owner)
    db.commit()
    db.refresh(db_owner)
    return db_owner


def create_owner_reading(db: Session, reading: ReadingIn, owner_id: int):
    db_reading = Reading(**reading.dict(), owner_id=owner_id)
    db.add(db_reading)
    db.commit()
    db.refresh(db_reading)
    return db_reading


def get_owner(db: Session, owner_id: int):
    return db.query(Owner).filter(Owner.id == owner_id).first()


def get_owner_by_name(db: Session, name: str):
    return db.query(Owner).filter(Owner.name == name).first()


def get_owners(db: Session, skip: int = 0, limit: int = 100):
    return db.query(Owner).offset(skip).limit(limit).all()
