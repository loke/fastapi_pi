import schedule

import uvicorn
from fastapi import FastAPI
from fastapi_utils.tasks import repeat_every

from app.config import ENV, HOST, PORT
from app.db import Base, engine
from app.readings.scheduler import clear_temp_task, init_temp_task
from app.routers import owner, reading, token


Base.metadata.create_all(bind=engine)
app = FastAPI()
app.include_router(owner.router, tags=['Owners'])
app.include_router(reading.router, tags=['Readings'])
app.include_router(token.router, tags=['Token'])


@app.on_event('startup')
def startup_event():
    init_temp_task()
    schedule.run_all()


@app.on_event('startup')
@repeat_every(seconds=60)
def run_pending():
    schedule.run_pending()


@app.on_event('shutdown')
def close_event():
    clear_temp_task()


if __name__ == '__main__':
    reload = ENV == 'dev'
    uvicorn.run('main:app', host=HOST, port=PORT, reload=reload)
