from typing import List

from fastapi import APIRouter, Body, Depends, HTTPException, Path, Query, status
from sqlalchemy.orm import Session

from app.authenticate import oauth2_scheme
from app.db import get_db
from app.schemas.owner import Owner, OwnerIn
from app.schemas.reading import Reading, ReadingIn
from app.services.owner import (
    create_owner,
    get_owner,
    get_owner_by_name,
    get_owners,
    create_owner_reading
)

router = APIRouter(prefix='/owners')


@router.post('/', response_model=Owner)
def create_owner(owner: OwnerIn = Body(...),
                 db: Session = Depends(get_db),
                 token: str = Depends(oauth2_scheme)):
    db_owner = get_owner_by_name(db, name=owner.name)
    if db_owner:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail='Owner already exists.')
    return create_owner(db=db, owner=owner)


@router.get('', response_model=List[Owner])
def read_owners(skip: int = Query(0, ge=0),
                limit: int = Query(100, ge=1),
                db: Session = Depends(get_db),
                token: str = Depends(oauth2_scheme)):
    owners = get_owners(db, skip=skip, limit=limit)
    return owners


@router.get('/{owner_id}', response_model=Owner)
def read_owner(owner_id: int = Path(..., ge=0),
               db: Session = Depends(get_db),
               token: str = Depends(oauth2_scheme)):
    db_owner = get_owner(db, owner_id=owner_id)
    if not db_owner:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail='Owner not found.')
    return db_owner


@router.post('/{owner_id}/readings/',
             response_model=Reading,
             status_code=status.HTTP_201_CREATED)
def create_reading_for_owner(owner_id: int = Path(..., ge=0),
                             reading: ReadingIn = Body(...),
                             db: Session = Depends(get_db),
                             token: str = Depends(oauth2_scheme)):
    return create_owner_reading(db=db, reading=reading, owner_id=owner_id)
